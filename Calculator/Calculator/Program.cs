﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Calculator();
        }

        static void Calculator()
        {
            Console.WriteLine("Input");
            var text = Console.ReadLine();
            string[] decimalNumbers = text.Split('+', '-', '*', '/', '(', ')');
            string[] operators = text.Split('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.');

            List<string> s = new List<string>();
            foreach (var item in operators)
            {
                if (!string.IsNullOrEmpty(item))
                {
                    s.Add(item);
                }
            }
            decimal intSum = 0;
            for (int i = 0; i < decimalNumbers.Length; i++)
            {
                if (i == 0)
                {
                    intSum = Convert.ToDecimal(decimalNumbers[i]);
                }
                if (i > 0)
                {
                    string oprator = s[i - 1];

                    switch (oprator)
                    {
                        case "+":
                            intSum = intSum + Convert.ToDecimal(decimalNumbers[i]);
                            break;
                        case "-":
                            intSum = intSum - Convert.ToDecimal(decimalNumbers[i]);
                            break;
                        case "*":
                            intSum = intSum * Convert.ToDecimal(decimalNumbers[i]);
                            break;
                        case "/":
                            intSum = intSum / Convert.ToDecimal(decimalNumbers[i]);
                            break;
                    }
                }
            }
            Console.WriteLine(Math.Round(intSum, 2).ToString());
        }
    }
}
